package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializerBase64Impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MultipleDAOTest {
    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");
    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }
    @Test
    public void testAddBadgeOnDatabase(){
        try {

            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase));

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            dao.addBadge(image);

            String serializedImage = reader.readLine();
            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;
            String[] data = serializedImage.split(";");
            System.out.println (data[0]);
            System.out.println (data[0] + "data 1 " + data[1] + " 2 " +  data[2]);
            // assertEquals(1, Integer.parseInt(data[0]));
            assertEquals(Files.size(image.toPath()), Long.parseLong(data[1]));
            assertEquals(encodedImage, data[2]);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            dao.addBadge(image2);
            String serializedImage2 = reader.readLine();
            String encodedImage2 = (String) serializer.serialize(image2);
            serializedImage2 = serializedImage2.replaceAll("\n","").replaceAll("\r","") ;
            String[] data2 = serializedImage2.split(";");

            assertEquals(2, Integer.parseInt(data2[0]));
            assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[1]));
            assertEquals(encodedImage2, data2[2]);
        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
}
