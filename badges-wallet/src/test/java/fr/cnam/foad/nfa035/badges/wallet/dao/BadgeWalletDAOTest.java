package fr.cnam.foad.nfa035.badges.wallet.dao;


import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.simpleaccess.ImageSerializerBase64Impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class BadgeWalletDAOTest {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * Teste l'ajout d'un badge
     */
    @Test
    public void testAddBadge(){
        try {

            File image = new File(RESOURCES_PATH + "petite_image.png");
            BadgeWalletDAO dao = new SingleBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            encodedImage = encodedImage.replaceAll("YII.*","");
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

    /**
     * Test la récupération d'un badge
     */
    @Test
    public void testGetBadge(){
        try {
            BadgeWalletDAO dao = new SingleBadgeWalletDAOImpl(RESOURCES_PATH + "wallet_full.csv");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
            dao.getBadge(memoryBadgeStream);
            byte[] deserializedImage = memoryBadgeStream.toByteArray();

            // Vérification 1
            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());
            //assertEquals(new String(originImage, StandardCharsets.UTF_8), new String(deserializedImage, StandardCharsets.UTF_8));
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

            // Vérification 2
            dao = new SingleBadgeWalletDAOImpl(RESOURCES_PATH + "wallet_full.csv");
            OutputStream fileBadgeStream = new FileOutputStream(extractedImage);
            dao.getBadge(fileBadgeStream);


            deserializedImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_extraite.png").toPath());
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
    @Test
    public void testAddBadgeOnDatabase(){
        try {

            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase));

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            dao.addBadge(image);

            String serializedImage = reader.readLine();
            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;
            String[] data = serializedImage.split(";");

            assertEquals(1, Integer.parseInt(data[0]));
            assertEquals(Files.size(image.toPath()), Long.parseLong(data[1]));
            assertEquals(encodedImage, data[2]);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            dao.addBadge(image2);
            String serializedImage2 = reader.readLine();
            String encodedImage2 = (String) serializer.serialize(image2);
            serializedImage2 = serializedImage2.replaceAll("\n","").replaceAll("\r","") ;
            String[] data2 = serializedImage2.split(";");

            assertEquals(2, Integer.parseInt(data2[0]));
            assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[1]));
            assertEquals(encodedImage2, data2[2]);
        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
    /**
     * Test la récupération d'un badge
     */
    @Test
    public void testGetBadgeFromDatabase(){
        try {
            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "wallet_full.csv");
            File extractedImage = new File(RESOURCES_PATH + "petite_image_extraite.png");

            ByteArrayOutputStream memoryBadgeStream = new ByteArrayOutputStream();
            ByteArrayOutputStream memoryBadgeStream2 = new ByteArrayOutputStream();
            dao.getBadge(memoryBadgeStream);
            byte[] deserializedImage = memoryBadgeStream.toByteArray();

            // Vérification 1
            byte [] originImage = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image.png").toPath());
            //assertEquals(new String(originImage, StandardCharsets.UTF_8), new String(deserializedImage, StandardCharsets.UTF_8));
            assertArrayEquals(Arrays.copyOfRange(originImage,0,originImage.length-2), deserializedImage);


            // Vérification 2
            dao.getBadge(memoryBadgeStream2);
            byte[] deserializedImage2 = memoryBadgeStream2.toByteArray();
            byte [] originImage2 = Files.readAllBytes(new File(RESOURCES_PATH + "petite_image_2.png").toPath());
            //assertEquals(new String(originImage, StandardCharsets.UTF_8), new String(deserializedImage, StandardCharsets.UTF_8));
            assertArrayEquals(Arrays.copyOfRange(originImage2,0,originImage2.length-2), deserializedImage2);



        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

}
