package fr.cnam.foad.nfa035.badges.wallet.dao;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public interface BadgeWalletDAO {
    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException;
    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException;
}
