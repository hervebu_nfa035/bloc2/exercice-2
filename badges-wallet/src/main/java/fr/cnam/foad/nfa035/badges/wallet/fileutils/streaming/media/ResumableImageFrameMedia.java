package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;
/**
 * Interface définissant le comportement attendu d'un media
 * permettant (potentiellement de stocker plusieurs images sérialisées)
 * Il doit être possible d'en obtenir un flux d'écriture.
 *
 * @param <T>
 */
public interface ResumableImageFrameMedia <T> extends ImageFrameMedia<T>  {
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
