package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;
/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux
 * avec l'objectif de serialiser plusieurs images dans le meme flux (fichier)
 */
public class ImageSerializerBase64DatabaseImpl extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {

    static int imageIndex = 0;
    /**
     * {@inheritDoc}
     *
     * @param source : fichier hébergeant les badges serialises
     * @return : flux d'entrée sur le fichier source
     * @throws FileNotFoundException
     */
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    /**
     * {@inheritDoc}
     * @param media :
     * @return : flux de sortie converti dans le type Base64OutputStream
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }

    /**
     * {@inheritDoc}
     * @param source : badge  à serialiser
     * @param media : serialisation media
     * @throws IOException
     */
    @Override
    public void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File) media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
          try (OutputStream os = media.getEncodedImageOutput()) {
             Writer writer = new FileWriter(String.valueOf(os));
             imageIndex++;
             //writer.write(......???..... ;  ;  ..........??........);
             writer.write("1" + imageIndex + ";" + size + ";");

             writer.flush();
             try (OutputStream eos = getSerializingStream(media)) {
                 getSourceInputStream(source).transferTo(eos);
                 writer.write("\n");
             }
             writer.flush();
         }
    }
}

