package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.*;

/**
 * Implémentation d'ImageFrame pour un simple Fichier comme canal
 */
public class ResumableImageFileFrame extends AbstractImageFrameMedia<File>  implements ResumableImageFrameMedia<File> {

    private BufferedReader reader;

    public ResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getEncodedImageOutput() throws FileNotFoundException {
        return new FileOutputStream(getChannel(), true);
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new FileInputStream(getChannel());
    }


    /**
     * {@inheritDoc}
     */
    //@Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null || !resume){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
    }

    /**
     * Implémentation d'ImageFrame pour un simple Fichier comme canal
     */
    public static class ImageFileFrame extends AbstractImageFrameMedia<File> {

        public ImageFileFrame(File walletDatabase) {
            super(walletDatabase);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public OutputStream getEncodedImageOutput() throws FileNotFoundException {
            return new FileOutputStream(getChannel());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public InputStream getEncodedImageInput() throws FileNotFoundException {
            return new FileInputStream(getChannel());
        }


    }
}
