package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;
/**
 * {@inheritDoc}
 *
 */
public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * {@inheritDoc}
     *
     */
        @Override
        public void deserialize(ResumableImageFrameMedia media) throws IOException {
            // 1. Récupération de l'instance de lecture séquentielle du fichier de base csv
            // getDeserializingStream((M) media).transferTo(getSourceOutputStream());
            BufferedReader br = media.getEncodedImageReader(true);
            // 2. Lecture de la ligne et parsage des différents champs contenus dans la ligne
            String[] data = br.readLine().split(";");
            // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
            try (OutputStream os = getSourceOutputStream()) {
                getDeserializingStream(data[2]).transferTo(os);
            }

        }
    /**
     * {@inheritDoc}
     *
     */
    @Override
    @SuppressWarnings("unchecked")
    public InputStream  getDeserializingStream(String data) throws IOException {
        return  new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
    //public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
    //    return (K) new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    //}
    /**
     * {@inheritDoc}
     *
     * @return le flux d'écriture
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

        try (OutputStream outputStream = this.sourceOutputStream = os) {
            System.out.println(" setSourceOutputStream");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
