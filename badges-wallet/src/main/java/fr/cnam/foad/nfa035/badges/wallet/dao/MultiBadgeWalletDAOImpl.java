package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class MultiBadgeWalletDAOImpl implements BadgeWalletDAO {
    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOImpl.class);


    private File walletDatabase;
    private ResumableImageFileFrame.ImageFileFrame media;
    // private ResumableImageFrameMedia<ByteArrayOutputStream> resumableImageFrameMedia;
    private DatabaseDeserializer<ResumableImageFrameMedia> deserializer = new ImageDeserializerBase64DatabaseImpl();


    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public MultiBadgeWalletDAOImpl(String dbPath) throws IOException {

        this.walletDatabase = new File(dbPath);
        this.media = new ResumableImageFileFrame.ImageFileFrame(walletDatabase);
        //this.resumableImageFrameMedia = new

    }


    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException{
        // ImageStreamingSerializer serializer2 = new ImageSerializerBase64StreamingImpl();
        ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{


        DatabaseDeserializer<ResumableImageFrameMedia> deserializer  = new ImageDeserializerBase64DatabaseImpl();
        // deserializer.deserialize(media);
    }
}
