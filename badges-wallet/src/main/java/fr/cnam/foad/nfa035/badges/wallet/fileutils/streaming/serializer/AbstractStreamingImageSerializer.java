package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import java.io.IOException;

/**
 *
 * Classe abstraite visant à structurer/guider le développement dee manière rigoureuse
 *
 * @param <S>
 * @param <M> Le Media de sérialisation,
 *           à vers lequel nous voulons sérialiser notre image en base 64
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {

    /**
     * Sérialise une image depuis un support quelconque vers un media quelconque
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public  void serialize(S source, M media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream(media));
    }

}
