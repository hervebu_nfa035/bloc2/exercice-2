package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.IOException;
import java.io.OutputStream;
/**
 * Interface définissant le comportement attendu d'un media (potentiellement une  image sérialisée)
 * et il doit être possible d'en obtenir un flux d'écriture.
 *
 * @param <T>
 */
public interface ImageFrameMedia <T>{
    /**
     * Permet d'obtenir le canal de distribution de notre image sérialisée, Fichier ,Flux ...
     *
     * @return le canal
     */
    public T getChannel() ;
    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    public abstract OutputStream getEncodedImageOutput() throws IOException;

}
