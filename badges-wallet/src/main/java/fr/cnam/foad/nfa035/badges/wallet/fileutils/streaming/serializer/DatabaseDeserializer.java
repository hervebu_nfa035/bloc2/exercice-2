package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;

/**
 * classe étendant BadgeDeserializer pour y ajouter une méthode permettant d'obtenir
 * le flux de deserailisation
 * @param <M>
 */
public interface DatabaseDeserializer <M extends ImageFrameMedia> extends BadgeDeserializer<M> {
    /**
     * Obtenir le flux de deserialisation
     * @param data
     * @return K extension input stream
     * @throws IOException
     */
    // <K extends InputStream> K getDeserializingStream(String data) throws IOException;
    InputStream  getDeserializingStream(String data) throws IOException;
}


