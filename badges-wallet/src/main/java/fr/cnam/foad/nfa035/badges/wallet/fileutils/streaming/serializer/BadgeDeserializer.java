package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Definition d'une interface permettant de manipuler un media contenant des bages à deserialiser.
 * fournit les methodes afin de "set/get" de la source à restituer et d'effectuer la deserialisation
 * @param <M>
 */
public interface BadgeDeserializer <M extends ImageFrameMedia> {
    void deserialize(M media) throws IOException;

    /**
     * récupérer un Flux d'écriture sur la source à restituer
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * setter le flux d'écriture
     * @param os : output stream
     *
     */
    <T extends OutputStream> void setSourceOutputStream(T os);

}
