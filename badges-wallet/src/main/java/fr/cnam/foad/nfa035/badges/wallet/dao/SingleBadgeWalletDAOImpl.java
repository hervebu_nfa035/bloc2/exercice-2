package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badge unique
 */
public class SingleBadgeWalletDAOImpl implements BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(SingleBadgeWalletDAOImpl.class);

    private File walletDatabase;
    private ResumableImageFileFrame.ImageFileFrame media;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public SingleBadgeWalletDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
        this.media = new ResumableImageFileFrame.ImageFileFrame(walletDatabase);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException{
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize(media);
    }

}
