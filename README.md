### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici l'énoncé du 2ème exercice noté de la 2ème session de ce projet.

---

# Listes, Ensembles et Couches logicielles
 ++ Intégration d'une couche logicielle transverse: commons/utils

## Contexte
* Au programme de ce cours: Listes, Ensembles, Couches logicielles
## Objectifs
* Mises en application:
 - [x] (Exercice 1) Homogénéisation du projet en suivant le standard Maven (Exercice 1)
 - [ ] Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [x] (Exercice 2) Ecriture/Lecture séquentielle mono-ligne dans le fichier texte, avec métadonnées simples, ID, Taille.
    - [ ] (Exercice 3) Lecture Du fichier texte en accès direct et stockage des métadonnées trouvées dans une Liste + Lecture d'un badge correspondant aux méta-données
 - [ ] (Exercice 4) Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [ ] Lecture Du fichier texte en accès direct et stockage des métadonnées trouvées dans un Set, donc... 
    - [ ] Ajout d'une fonction de hachage dans les métadonnées 
    - [ ] Ajout d'une gestion d'exception lors de l'ajout de badge pour garantir l'unicité des badges.


## Consignes de l'Exercice 2

Abordons à présent l'exercice 2 de cette Séance, avec l'Ecriture séquentielle mono-ligne dans le fichier texte, avec métadonnées simples, ID, Taille.

### Lecture/Ecriture d'un badge avec ses méta-données au format CSV
    
Pour ce faire, respecter les étapes suivantes:

#### Couche transverse d'abstraction

```plantuml
@startuml

title __Design de l'Abstraction du Media de sérialisation__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageByteArrayFrame {
            + ImageByteArrayFrame()
            + getEncodedImageInput()
            + getEncodedImageOutput()
        }
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame {
            + ImageFileFrame()
            + getEncodedImageInput()
            + getEncodedImageOutput()
        }
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame {
            - reader : BufferedReader
            + ResumableImageFileFrame()
            + getEncodedImageInput()
            + getEncodedImageOutput()
            + getEncodedImageReader()
        }
      }
    }
  }
  

  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageByteArrayFrame -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      abstract class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia {
          + AbstractImageFrameMedia()
          + getChannel()
          {abstract} + getEncodedImageInput()
          {abstract} + getEncodedImageOutput()
          + setChannel()
          # AbstractImageFrameMedia()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia {
          {abstract} + getChannel()
          {abstract} + getEncodedImageOutput()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace media {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia {
          {abstract} + getEncodedImageReader()
      }
    }
  }


  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia o-- T : channel
@enduml
```

 - [ ] A l'aide du diagramme ci-dessus, refactorer la couche transverse afin de mieux structurer l'abstraction du media de sérialisation.
   - [ ] Coder (Packaging + Javadoc), puis implémenter, **en réutilisant le code existant**, l'interface **ImageFrameMedia** qui suit:
```java
public interface ImageFrameMedia<T> {
    T getChannel() ;
    OutputStream getEncodedImageOutput() throws IOException;
}
```
   - [ ] Coder (Packaging + Javadoc), puis implémenter **par une nouvelle classe**, l'interface **ResumableImageFrameMedia** qui suit:

```java
public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {
    BufferedReader getEncodedImageReader(boolean resume) throws IOException;   
}
```
   - [ ] Facultatif mais conseillé en cas de blocage dans la suite du TD: écrire un test unitaire sur la nouvelle classe implémentée, afin de s'assurer que ce composant logiciel ainsi modifié génère bien, lorsqu'on lui soumet des entrées arbitraires, les sorties attendues. 

#### Couche technique (transverse également) de sérialisation


```plantuml
@startuml

title __Design de la couche transverse/technique de sérialisation__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl {
            - sourceOutputStream : OutputStream
            + ImageDeserializerBase64DatabaseImpl()
            + deserialize()
            + getDeserializingStream()
            + getSourceOutputStream()
            + setSourceOutputStream()
        }
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl {
            + ImageDeserializerBase64StreamingImpl()
            + getDeserializingStream()
        }
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl {
            + getSerializingStream()
            + getSourceInputStream()
            + serialize()
        }
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      namespace impl {
        class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl {
            + getSerializingStream()
            + getSourceInputStream()
        }
      }
    }
  }
  

  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer

  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer -up-|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer
  fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer .up.|> fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer

 namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      abstract class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer {
          - sourceOutputStream : OutputStream
          + deserialize()
          + getSourceOutputStream()
          + setSourceOutputStream()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      abstract class fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer {
          + serialize()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.BadgeDeserializer {
          {abstract} + deserialize()
          {abstract} + getSourceOutputStream()
          {abstract} + setSourceOutputStream()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer {
          {abstract} + getDeserializingStream()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer {
          {abstract} + getDeserializingStream()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
    namespace serializer {
      interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer {
          {abstract} + getSerializingStream()
          {abstract} + getSourceInputStream()
          {abstract} + serialize()
      }
    }
  }

@enduml
```

- [ ] A l'aide du diagramme ci-dessus, refactorer la couche transverse afin de mieux structurer l'abstraction technique de sérialisation.
   - [ ] Mais tout d'abord, afin de permettre une désérialisation d'images vers de multiples support (lecture successive de plusieurs badges dans un même csv), ajouter la méthode suivante à l'interface **ImageStreamingDeserializer**, puis **l'implémenter dans le package indiqué**.
```java
public interface ImageStreamingDeserializer<M extends ImageFrameMedia> {
   //...
    <T extends OutputStream> void setSourceOutputStream(T os);
}
```
   - [ ] A l'aide de la définition de l'interface suivante, qui rationalise le contrat de ce composant, simplifier l'arbre d'héritage existant entre interfaces et classes (Abstraites ou non):
```java
public interface BadgeDeserializer<M extends ImageFrameMedia> {
    void deserialize(M media) throws IOException;
    <T extends OutputStream> T getSourceOutputStream();
    <T extends OutputStream> void setSourceOutputStream(T os);
}
```
   - Implémenter par ailleurs La classe suivante (à compléter) afin de gérer à présent une sérialisation multi lignes, c'est-à-dire permettant d'ajouter un badge à la fin du fichier csv, sous la forme d'une seule ligne en respectant le format <**ID;Taille en Octets;image en Bas64**> :
```java
public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {
    public InputStream getSourceInputStream(File source) throws FileNotFoundException { return new FileInputStream(source); }
    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream(.... ??
    }
    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.????) {
            Writer writer = ????;
            writer.write(......???..... ;  ;  ..........??........);
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }

}
```
   - [ ] Valider l'écriture multi-lignes à l'aide de cette méthode de test dans votre classe ...DAOTest:
```java
    @Test
    public void testAddBadgeOnDatabase(){
        try {

            BadgeWalletDAO dao = new MultiBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");
            BufferedReader reader = new BufferedReader(new FileReader(walletDatabase));

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            dao.addBadge(image);

            String serializedImage = reader.readLine();
            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;
            String[] data = serializedImage.split(";");

            assertEquals(1, Integer.parseInt(data[0]));
            assertEquals(Files.size(image.toPath()), Long.parseLong(data[1]));
            assertEquals(encodedImage, data[2]);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            dao.addBadge(image2);
            String serializedImage2 = reader.readLine();
            String encodedImage2 = (String) serializer.serialize(image2);
            serializedImage2 = serializedImage2.replaceAll("\n","").replaceAll("\r","") ;
            String[] data2 = serializedImage2.split(";");

            assertEquals(2, Integer.parseInt(data2[0]));
            assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[1]));
            assertEquals(encodedImage2, data2[2]);
        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }
```

   - [ ] Nous allons à présent aborder la partie la plus complexe, la désérialisation. Pour avancer, introduire ensuite l'interfaces suivante pour une lecture, ligne par ligne, à partir du format csv (qui constitue notre base de données pour le moment):
```java
public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {
    <K extends InputStream> K getDeserializingStream(String data) throws IOException;
}
```
   - [ ] Puis l'implémenter, toujours avec Base64, selon le diagramme (nommage et package), en vous aidant éventuellement de ceci:
```java
public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {
    ...

    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        // 1. Récupération de l'instance de lecture séquentielle du fichier de base csv
        // 2. Lecture de la ligne et parsage des différents champs contenus dans la ligne
        // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
    }
}
...

@Override
public InputStream getDeserializingStream(String data) throws IOException {
    return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
}
```
 - [ ] Rétroactivement, ajouter une ligne de commentaire explicatif donnant sens à chaque ligne de code prise depuis cet énoncé, et veiller aussi à fournir une Javadoc complète dans le code.

#### Couche DAO


```plantuml
@startuml

title __Design de la couche DAO__\n

    namespace fr.cnam.foad.nfa035.badges.wallet.dao {
        interface fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO {
            {abstract} + addBadge()
            {abstract} + getBadge()
        }

        namespace impl {
            class fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl {
                {static} - LOG : Logger
                - deserializer : ImageStreamingDeserializer<ResumableImageFrameMedia>
                - walletDatabase : File
                + MultiBadgeWalletDAOImpl()
                + addBadge()
                + getBadge()
            }
        }
    }


    namespace fr.cnam.foad.nfa035.badges.wallet.dao {
        namespace impl {
            class fr.cnam.foad.nfa035.badges.wallet.dao.impl.SingleBadgeWalletDAOImpl {
                {static} - LOG : Logger
                - walletDatabase : File
                + SingleBadgeWalletDAOImpl()
                + addBadge()
                + getBadge()
            }
        }
    }




    namespace fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming {
        namespace media {
            interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia {
                {abstract} + getEncodedImageReader()
            }
            interface fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia {
                {abstract} + getChannel()
                {abstract} + getEncodedImageOutput()
            }
        }
    }

    fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO
    fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl o-- fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia : media
    fr.cnam.foad.nfa035.badges.wallet.dao.impl.SingleBadgeWalletDAOImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO
    fr.cnam.foad.nfa035.badges.wallet.dao.impl.SingleBadgeWalletDAOImpl o-- fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia : media

@enduml
```
- [ ] A l'aide du diagramme ci-dessus, refactorer la couche DAO en définissant et en exploitant l'interface **BadgeWalletDAO**.
- [ ] Implémenter les classes **MultiBadgeWalletDAO** et **ImageDeserializerBase64DatabaseImpl** pour permettre la lecture en séquence des badges de notre fichier csv, tout en vous aidant également de ceci:

```java
@Override
public void deserialize(ResumableImageFrameMedia media) throws IOException {
    BufferedReader br = media.getEncodedImageReader(true);
    String[] data = br.readLine().split(";");
    try (OutputStream os = getSourceOutputStream()) {
        getDeserializingStream(data[2]).transferTo(os);
    }
}
```
- [ ] Fournir un test Unitaire concluant et significatif pour la désérialisation, en s'inspirant du précédent.
- [ ] Ajouter le résultat du test précédent (.csv) comme échantillon d'entrée de ce nouveau TU, puis le désérialiser et fournir des assertions sur le contenu.
- [ ] Et pour finir, apporter la preuve du bon fonctionnement des tests (saisie d'écran)

----

### Ressources ###

Vous voudrez probablement vous aider de ce nouveau [Media](badges-wallet/src/main/java/fr/cnam/foad/nfa035/badges/wallet/fileutils/streaming/media/impl/ResumableImageFileFrame.java) disponible... mais vous l'aviez sûrement deviné ;)


